#!/usr/bin/env bash

# Based off cargo's completion script
#
# Permission is hereby granted, free of charge, to any
# person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without
# limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software
# is furnished to do so, subject to the following
# conditions:
# 
# The above copyright notice and this permission notice
# shall be included in all copies or substantial portions
# of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
# ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
# TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

_website_completions() {
    local cur prev words cword
    _get_comp_words_by_ref cur prev words cword

    COMPREPLY=()

    local subcommands='help -h --help create enable disable edit-config remove'
    # xargs turns its input into space-seperated output
    local sites_available='$(ls /etc/nginx/sites-available | xargs)'

    if [[ $cword -eq 1 ]]; then
        # Completion before or at the command.
        COMPREPLY=( $( compgen -W "$subcommands" -- "$cur" ) )
    else
        COMPREPLY=( $( compgen -W "$sites_available" -- "$cur" ) )
    fi

    return 0
} &&
complete -F _website_completions website
