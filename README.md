# website
A simple website administration tool I threw together to make my life easier.
This is mostly intended for personal use.

## Basic setup
```console
$ git clone https://codeberg.org/PatchMixolydic/website.git
$ cd website
$ mkdir -p ~/.local/bin ~/.local/share
$ ln -s "$(pwd)/bin/website" ~/.local/bin/website
$ ln -s "$(pwd)/share/website" ~/.local/share/website
```

You can configure `website` by copying `etc/website/config` to `~/.config/website/config`.
Configuration variables are also read from `/etc/website/config` in case you want to set up a systemwide
installation.

If you use Bash, you can source `share/website/completions.bash` to set up tab completions.

If you're on a system that doesn't follow [the Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html),
you might need to set the `WEBSITE_CONFIG_DIRECTORY` environment variable before running `website`.
